const Discord = require('discord.js');
const bot = new Discord.Client();
const token = require('./token.js');

var randomWords = require('random-words');


const PREFIX = "!";

bot.on('ready', () => {
    console.log('This bot is online !');
})

bot.on('message', msg => {
    let args = msg.content.substring(PREFIX.length).split(" ");

    switch (args[0]) {
        case 'random':
            // Retrieve the second parameters that is supposed to be the number of words wanted
            let number = args[1];
            console.log(args[1]);

            // If incorrect or unspecified the number is set to 1 word
            if (number === 1 || !number || number < 0) {
                // Respond with a random word
                msg.reply(randomWords());
            } else {
                // Respond with the wanted number of random words
                msg.reply(randomWords(parseInt(number)));
            }
            break;
        case 'retard':
            let txt = msg.content.substring(PREFIX.length + 6);
            let result = "";
            let up = false;
            for (let i = 0; i < txt.length; i++) {
                if (up === false) {
                    result += txt[i].toUpperCase();
                    up = true;
                } else {
                    result += txt[i].toLowerCase();
                    up = false;
                }
            }
            msg.reply(result);
            break;
    }


})

bot.login(token);
